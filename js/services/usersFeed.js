define(['ngapp'], function (ngapp) {
    ngapp.factory('UsersFeed', function () {
        return {
            users: [
                { name: 'A 1' },
                { name: 'B 2' },
                { name: 'C 3' }
            ]
        };
    });
});
