define(['ngapp'], function (ngapp) {
    ngapp.directive('mouseOver', function () {
        return {
            controller: function ($scope) {
                $scope.currentValue = null;
            },
            resctrict: 'A',
            scope: {
                reactTo: '=',
                mouseOverValue: '='
            },
            template: '<span>{{mouseOverValue}}</span>',
            link: function (scope, element, attrs) {
                element.bind('mouseover', function (e) {
                    scope.$apply(function () {
                        scope.currentValue = scope.reactTo;
                    });
                });
                element.bind('mouseleave', function (e) {
                    scope.$apply(function () {
                        scope.currentValue = null;
                    });

                });

                scope.$watch('currentValue', function (value) {
                    if(value && value.name === scope.mouseOverValue) {
                        element.addClass('yellow-active');
                    } else {
                        element.removeClass('yellow-active');
                    }
                });
            }
        }
    });
});
