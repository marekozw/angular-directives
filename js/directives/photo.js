define(['ngapp'], function (ngapp) {
	ngapp.directive('photo', function () {
	    return {
	        restrict: 'E',
	        replace: true,
	        scope: {
	            photoSrc: '@',
	            caption: '@'
	        },
	        templateUrl: 'templates/photo.html'
	    };
	});
});