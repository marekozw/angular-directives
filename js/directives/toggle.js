define(['ngapp'], function (ngapp) {
	ngapp.directive('toggle', function () {
	    return {
	        restrict: 'A',
	        scope: {
	            toggle: '=',
	            activeClass: '@'
	        },
	        link: function (scope, element, attrs) {
	            scope.$watch('toggle', function (value) {
	                element.toggleClass(scope.activeClass, value);
	            });
	            element.bind('click', function () {
	                scope.$apply(function () {
	                    scope.toggle = !scope.toggle;
	                });
	            });
	        }
	    };
	});
})