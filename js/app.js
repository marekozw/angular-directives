require.config({
    paths: {
        'angular': '../lib/angular',
    },
    shim: {
        'angular': {
            exports: 'angular'
        }
    }
});

require(['angular', 'loader'], function (angular) {
    angular.element(document).ready(function () {
        angular.bootstrap(document, ['myApp']);
    });
});
