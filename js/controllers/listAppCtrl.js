define(['ngapp'], function (ngapp) {
    ngapp.controller('ListAppCtrl', ['$scope', 'UsersFeed',
        function ($scope, UsersFeed) {
            $scope.users = UsersFeed.users;
        }
    ]);
});

