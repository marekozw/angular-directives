define(['ngapp'], function (ngapp) {
	ngapp.controller('PhotoCtrl', ['$scope', 
		function ($scope) {
		    $scope.photo = {
		        url: 'images/ariel.jpg',
		        date: new Date().toString('dd-MM-yyyy')
	    	};
	    }
	]);
});