define(['ngapp'], function (ngapp) {
	ngapp.controller('ToggleCtrl', ['$scope',
		function ($scope) {
		    $scope.preferences = {
		        showDetails : false
		    };
		}
	]);
});